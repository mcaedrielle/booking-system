let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}
let firstName = document.querySelector('#firstName');
let lastName = document.querySelector('#lastName');
let mobileNo = document.querySelector('#mobileNo');
let email = document.querySelector('#email');

//fetch user details
fetch(`${url}/api/users/details`,{
	method: "GET",
	headers: {
		'Content-Type' : 'application/json',
		'Authorization' : localStorage.getItem('token')
	}
})
.then( res => res.json())
.then( data => {
	console.log('data', data);
	if(!data) {
		alert('something went wrong')
	}

	firstName.innerHTML = `<strong> Name </strong> : ${data.firstName} ${data.lastName} `
	mobileNo.innerHTML = `<strong>Contact No.</strong> : ${data.mobileNo}`
	email.innerHTML = `<strong>Email</strong> : ${data.email}`

	let courses = data.enrollments

	let courseCard = (params) => {
		console.log(params)
		return (`
			<div class="col-md-6 my-3">
				<div class="card border-dark">
					<div class="card-body">
						<h5 class="card-title"><strong>${params.name} </strong></h5>
						<p class="card-text text-center">
							${params.description}
						</p>
					</div>
				</div>
			</div>
	`)
	} 

	async function fetchCourse(course) {
    	const response = await fetch(`${url}/api/courses/${course.courseId}`);
    	const courseResponse = await response.json()
    	return courseCard(courseResponse)
    }


    let courseData = '';

    async function main() {
    	if(courses.length < 1){
    		courseData = "No courses available";
    	} else {
    		for (let i=0; i<courses.length; i++) {
    			courseData += await fetchCourse(courses[i])
    		}
	  }

	  let coursesContainer = document.querySelector('#coursesContainer');
	  coursesContainer.innerHTML = courseData;
    }

    main();

})

