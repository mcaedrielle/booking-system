let adminUser = localStorage.getItem("isAdmin");
console.log('isAdmin', adminUser);
let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

// fetch
fetch(url + '/api/courses')
.then( res => res.json())
.then(data => {
	// console.log(data) //array
	let coursesContainer = document.querySelector('#coursesContainer');
	let addCourse = document.querySelector('#addCourse');

	if (adminUser === "false" || !adminUser ) {
		addCourse.innerHTML = `<a href="./register.html" class="btn btn-outline-dark"> Not a user yet? Register Now or Select a Course below</a>`
	} else {
		addCourse.innerHTML = `<a href="./addCourse.html" class="btn btn-outline-dark"> Add Course</a>`
	}


	let cardFooter = (course) => {
		console.log('course', course)

		if (adminUser === "false" || !adminUser) {
			return `<a href="./course.html?courseId=${course._id}"" class="btn btn-block border-dark">Select Course</a>`
		} else {
			if (course.isActive) {
				return `
					<a href="./editCourse.html?courseId=${course._id}" class="btn btn-block border-dark editButton">Edit</a>
					<a href="./deleteCourse.html?courseId=${course._id}" class="btn btn-danger border-dark btn-block dangerButton">Archive course</a>
				`
			}		
			return `
				<a href="./editCourse.html?courseId=${course._id}" class="btn btn-block border-dark editButton">Edit</a>
				<a href="./reactivateCourse.html?courseId=${course._id}" class="btn btn-warning border-dark btn-block">Reactivate course</a>
			`
		}
	}
	let courseCard = (params) => (`
		<div class="col-md-6 my-3">
			<div class="card border-dark">
				<div class="card-body">
					<h5 class="card-title">${params.name}</h5>
					<p class="card-text text-left">
						${params.description}
					</p>
					<div class="card-text text-right">
						&#8369; ${params.price}
					</div>
				</div>
				<div class="card-footer bg-white border-dark">
					${cardFooter(params)}
				</div>
			</div>
		</div>
	`)
	let courseData;
	if(data.length < 1){
		courseData = "No courses available";
	} else {
		courseData = data.map( course => {
			return courseCard(course);
		}).join("")
	}
	 

	coursesContainer.innerHTML = courseData;
})
