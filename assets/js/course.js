let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')
let adminUser = localStorage.getItem("isAdmin");	

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollContainer = document.querySelector('#enrollContainer');

fetch(`${url}/api/courses/${courseId}`)
.then( res => res.json())
.then( data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price
	enrollContainer.innerHTML = `<button id="enrollButton" href="./enrollCourse.html" class="btn border-dark p-16">Enroll</button>`


	let studentCard = (params) => {
		return (`
			<div class="col-md-6 my-3">
				<div class="card bg-white border-dark">
					<div class="card-body">
						<p>${params.firstName}</p>
						<p>${params.lastName}</p>

					</div>
				</div>
			</div>
	`)
	} 

	async function fetchStudent(student) {
    	const response = await fetch(`${url}/api/users/${student.userId}`,{
				method: "GET",
				headers: {
					'Content-Type' : 'application/json',
					'Authorization' : localStorage.getItem('token')
				}
			})
    	const studentResponse = await response.json();

    	return studentCard(studentResponse)
    }

    let students = data.enrollees;
    let studentData = '';

    async function main() {
    	if(students.length < 1){
    		studentData = "No students enrolled to the course available";
    	} else {
    		for (let i=0; i<students.length; i++) {
    			studentData += await fetchStudent(students[i])
    		}
	  }

	  let studentsContainer = document.querySelector('#studentsContainer');
	  studentsContainer.innerHTML = studentData;
    }

	if (adminUser === "true") {
    	main();
	}


	let enrollButton = document.querySelector('#enrollButton')
	enrollButton.addEventListener('click', () => {
		fetch(`${url}/api/users/enroll`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			},
			body: JSON.stringify({ courseId })
		})
		.then( res => res.json())
		.then( res => {
			if(data === true) {
				alert('Thank you for enrolling! See you!')
				window.location.replace('./courses.html')
			} else {
				alert("Something went wrong")
			}
		})
	})
})