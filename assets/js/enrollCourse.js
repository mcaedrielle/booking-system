let isLoggedIn = localStorage.length > 0;

let params = new URLSearchParams(window.location.search);
let id = params.get('courseId')

fetch(`${url}/api/users/enroll`,{
	method: "POST",
	headers: {
		'Authorization' : localStorage.getItem('token')
	},
	body: JSON.stringify({
		courseId: id
	})
})
.then( res => res.json())
.then( data => {
	console.log('data',data)
	if(data === true) {
		window.location.replace('./course.html')
	} else {
		console.log('data', data)
		alert('something went wrong')
	}
})
