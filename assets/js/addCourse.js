let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

let createCourse = document.querySelector('#createCourse')
createCourse.addEventListener('submit', e => {
	e.preventDefault();

	// let params = new URLSearchParams(window.location.search);

	// let courseId = params.get('courseId')


	let courseName = document.querySelector("#courseName").value;
	let coursePrice = document.querySelector("#coursePrice").value;
	let courseDescription = document.querySelector("#courseDescription").value;
	// get all field values
	// send data to the backend
	//you need token to send the data

		// let nameVal = name.value;
		// let priceVal = price.value;
		// let descVal = description.value;

		fetch(`${url}/api/courses`,{
			method : 'POST',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			},
			body: JSON.stringify({
				name: courseName,
				description: courseDescription,
				price: coursePrice
			})
		})
		.then( res => res.json())
		.then( data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("something went wrong")
			}
		})

	// if the fetch result is true
	//redirect use to the course.html
	// else alert(something went wrong)
})