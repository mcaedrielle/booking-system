let adminUser = localStorage.getItem("isAdmin");
let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="#" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./pages/logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./pages/register.html" class="nav-link"> Register </a>
		</li>

		<li class="nav-item">
			<a href="./pages/login.html" class="nav-link"> Login </a>
		</li>

		<span id="navSession"></span>
	`)
}