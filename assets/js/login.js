let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

let loginForm = document.querySelector('#logInUser');

loginForm.addEventListener('submit', e => {
	e.preventDefault();

	let email = document.querySelector('#userEmail').value;
	let password = document.querySelector('#password').value;

	fetch(url + '/api/users/login',{
		method: "POST",
		body : JSON.stringify({
			email : email,
			password : password
		}),
		headers : {
			'Content-Type' : 'application/json'
		}
	})
	.then( res => res.json())
	.then( data => {
		// console.log(data)
		if (data.accessToken) {
			localStorage.setItem('token',`Bearer ${data.accessToken}`)

			fetch(url + '/api/users/details',{
				headers: {
					Authorization: localStorage.getItem('token')
				}
			})
			.then( res => res.json())
			.then( data => {
				console.log(data)

				localStorage.setItem("id", data._id)
				localStorage.setItem("isAdmin", data.isAdmin)
				window.location.replace("./courses.html")

			})
		} else {
			alert("Something went wrong")
		}
	})
})