let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

console.log(window.location.search)

let params = new URLSearchParams(window.location.search);
console.log(params);
console.log(params.has('courseId'))

let courseId = params.get('courseId')

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(url + `/api/courses/` +courseId)
.then( res => res.json())
.then( data => {
	console.log(data)

	name.value = data.name
	price.value = data.price
	description.value = data.description

	let editCourse = document.querySelector('#editCourse');
	editCourse.addEventListener("submit", e => {
		e.preventDefault();

		let nameVal = name.value;
		let priceVal = price.value;
		let descVal = description.value;

		fetch(`${url}/api/courses`,{
			method : 'PUT',
			headers : {
				'Content-Type' : 'application/json',
				'Authorization' : localStorage.getItem('token')
			},
			body: JSON.stringify({
				courseId,
				name: nameVal,
				description: descVal,
				price: priceVal
			})
		})
		.then( res => res.json())
		.then( data => {
			if (data === true) {
				window.location.replace('./courses.html')
			} else {
				alert("something went wrong")
			}
		})
	})
})