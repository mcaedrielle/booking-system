let isLoggedIn = localStorage.length > 0;

let params = new URLSearchParams(window.location.search);
let courseId = params.get('courseId')

fetch(`${url}/api/courses/${courseId}`,{
	method: "PUT",
	headers: {
		'Authorization' : localStorage.getItem('token')
	}
})
.then( res => res.json())
.then( data => {
	if(data === false) {
		window.location.replace('./courses.html')
	} else {
		console.log('data',data)
		alert('something went wrong')
	}
})
