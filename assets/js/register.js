let isLoggedIn = localStorage.length > 0;

let navLinks = document.querySelector('#navLinks');

if (isLoggedIn) {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>

		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Sign Out </a>
		</li>

		<span id="navSession"></span>
	`)
} else {
	navLinks.innerHTML = (`
		<li class="nav-item">
			<a href="./../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item  ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>

		<li class="nav-item">
			<a href="./register.html" class="nav-link"> Register </a>
		</li>

		<span id="navSession"></span>
	`)
}

let registerForm = document.querySelector('#registerUser')

registerForm.addEventListener("submit", e => {
	e.preventDefault();

	let firstName = document.querySelector('#firstName').value
	let lastName = document.querySelector('#lastName').value
	let mobileNo = document.querySelector('#mobileNumber').value
	let email = document.querySelector('#userEmail').value
	let password1 = document.querySelector('#password1').value
	let password2 = document.querySelector('#password2').value

	let isFormComplete = password1 === password2 &&
	password1 !== "" &&
	password2 !== "" &&
	firstName !== "" &&
	lastName !== "" &&
	email !== "" &&
	mobileNo.length === 11

	// console.log(isFormComplete)
	if (isFormComplete) {
		// check for duplicate email
		fetch(url + `/api/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email
			})
		})
		.then( res => res.json())
		.then( data => {
			// console.log(data)
			if (data === false ) {
				fetch(url + `/api/users`, {
					method: 'POST',
					headers : {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						firstName,
						lastName,
						email,
						password: password1,
						mobileNo
					})
				})
				.then( res => res.json())
				.then( data => {
					if (data === true) {
						alert("registered successfully")
						window.location.replace('./login.html');
					} else {
						alert("something went wrong")
					}
				})
			} else {
				alert("email already in use")
			}
		})
	} else {
		alert("something went wrong")
	}

})