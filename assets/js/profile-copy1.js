let firstName = document.querySelector('#firstName');
let lastName = document.querySelector('#lastName');
let mobileNo = document.querySelector('#mobileNo');
let email = document.querySelector('#email');

//fetch user details
fetch(`${url}/api/users/details`,{
	method: "GET",
	headers: {
		'Content-Type' : 'application/json',
		'Authorization' : localStorage.getItem('token')
	}
})
.then( res => res.json())
.then( data => {
	if(data) {
		firstName.innerHTML = data.firstName
		lastName.innerHTML = data.lastName
		mobileNo.innerHTML = data.mobileNo
		email.innerHTML = data.email

		let courses = data.enrollments;

		let courseCard = (params) => (`
			<div class="col-md-6 my-3">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">${params.name}</h5>
						<p class="card-text text-left">
							${params.description}
						</p>
					</div>
				</div>
			</div>
		`)

		if (courses.length < 1) {
			courseData = "No courses available";	
		} else {
			// console.log(courses);
			courseData = courses.map((course, index) => {
				fetch(`${url}/api/courses/${course.courseId}`)
				.then(res => res.json())
				.then(data => {
					console.log('course', data);
				})
			})
		}

		let coursesContainer = document.querySelector('#coursesContainer');
		coursesContainer.innerHTML = courseData;

	} else {	
		// console.log(data)
		alert('something went wrong')
	}


})

